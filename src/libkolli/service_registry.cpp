#include "service_registry.hpp"

#include <stdexcept>

namespace kolli
{

  ServiceRegistry::ServiceRegistryType ServiceRegistry::s_registry;

  bool ServiceRegistry::registerService(std::string service, FetchFunction func)
  {
    return s_registry.insert({ service, func }).second;
  }

  ServiceRegistry::FetchType ServiceRegistry::fetch(std::string const & service, std::string const & trackingID)
  {
    if (auto it = s_registry.find(service); it != s_registry.end())
      return it->second(trackingID);

    auto err = service + ": Unknown service";
    throw std::runtime_error(err);
    return {};
  }

  std::vector<std::string> ServiceRegistry::list()
  {
    std::vector<std::string> list;
    for (auto [ service, function ] : s_registry)
      list.push_back(service);
    return list;
  }

}

#pragma once

#include <memory>
#include <string>
#include <vector>

namespace kolli
{
  struct Cookie
  {
    std::string name;
    std::string value;
  };

  class CurlSession
  {
    public:

      CurlSession();

      std::string get(std::string const & url, std::vector<std::string> const & headers);
      std::string post(std::string const & url, std::string const& postData, std::vector<std::string> const & headers);

      std::vector<Cookie> getCookies() const;
    private:

      void reset();

      struct CurlCleanup { void operator()(void * curl) const; };
      std::unique_ptr<void, CurlCleanup> m_handle;
  };
}

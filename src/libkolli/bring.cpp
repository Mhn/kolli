#include "service_registry.hpp"
#include <nlohmann/json.hpp>

namespace kolli
{
  static std::string buildUrl(std::string const & id)
  {
    return "https://tracking.bring.com/api/v2/tracking.json?q=" + id;
  }

  static Event getEventInfo(nlohmann::json const & json)
  {
    return Event{
      .description = json["description"],
      .date = parseDate(json["dateIso"]),
      .location = {
        .name = json["city"],
      },
    };
  }

  static std::optional<ParcelInfo> getParcelInfo(nlohmann::json const & json)
  {
    ParcelInfo p;
    for (auto const & cs : json["consignmentSet"])
      for (auto const & ps : cs["packageSet"])
      {
        p.id = ps["packageNumber"];
        p.estimatedArrival = parseDate(ps["dateOfEstimatedDelivery"]);
        for (auto const & es : ps["eventSet"])
          p.events.push_back(getEventInfo(es));
      }
    std::reverse(p.events.begin(), p.events.end());
    return p;
  }

  static std::optional<ParcelInfo> fetch(std::string const & trackingID)
  {
    try
    {
      return getParcelInfo(nlohmann::json::parse(fetchUrl(buildUrl(trackingID))));
    }
    catch (...)
    {
      return {};
    }
  }

  [[gnu::used]] ServiceRegistration bring_service("bring", fetch);
}

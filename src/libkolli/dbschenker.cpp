#include "service_registry.hpp"

#include <tinyxml2.h>

namespace kolli
{
  static std::string buildUrl(std::string const & id)
  {
    return "http://privpakportal.schenker.nu/TrackAndTrace/packagexml.aspx?packageid=" + id;
  }

  static std::optional<Event> getEventInfo(tinyxml2::XMLElement const * xml)
  {
    return Event{
      .description = xml->FirstChildElement("description")->GetText(),
      .date = parseDate(xml->FirstChildElement("date")->GetText()),
//      .location = {
//        .name = xml->FirstChildElement("location")->GetText(),
//      },
    };
  }

  // std::reverse for older compilers
  template<class BidirIt>
  static void reverse(BidirIt first, BidirIt last)
  {
    while ((first != last) && (first != --last)) {
      std::iter_swap(first++, last);
    }
  }

  static std::optional<ParcelInfo> getParcelInfo(tinyxml2::XMLElement const * xml)
  {
    ParcelInfo p;
    for (auto el = xml->FirstChildElement("body")->FirstChildElement("parcel")->FirstChildElement("event"); el; el = el->NextSiblingElement("event"))
      if (auto e = getEventInfo(el))
        p.events.push_back(*e);
    kolli::reverse(p.events.begin(), p.events.end());
    return p;
  }

  static std::optional<ParcelInfo> fetch(std::string const & trackingID)
  {
    try
    {
      tinyxml2::XMLDocument doc;
      auto str = fetchUrl(buildUrl(trackingID));
      doc.Parse(str.c_str());
      return getParcelInfo(doc.RootElement());
    }
    catch (...)
    {
      return {};
    }
  }

  [[gnu::used]] ServiceRegistration dbschenker_service("dbschenker", fetch);
}

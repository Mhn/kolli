#include "service_registry.hpp"
#include "curl_session.hpp"
#include <nlohmann/json.hpp>

namespace
{
  using namespace kolli;

  Event getEventInfo(nlohmann::json const & json)
  {
    auto location = json["Location"];

    return Event{
      .description = json["Status"],
      .date = parseDate(json["Time"]),
      .location = {
        .name = (location.is_null() || location == "") ? "N/A" : location
      },
    };
  }

  std::optional<ParcelInfo> getParcelInfo(nlohmann::json const & json)
  {
    ParcelInfo p;
    for (auto const & e : json["SearchResults"][0]["OrderStatuses"])
      p.events.push_back(getEventInfo(e));
    return p;
  }

  std::optional<ParcelInfo> lastbilsstationFetch(std::string const & trackingID)
  {
    try
    {
      return getParcelInfo(nlohmann::json::parse(CurlSession{}.get("https://boka.lastbilsstation.se/Opter/api/TT/internal/" + trackingID, {})));
    }
    catch (...)
    {
      return {};
    }
  }
}

namespace kolli {
  [[gnu::used]] ServiceRegistration lastbilsstation_service("lastbilsstation", lastbilsstationFetch);
}

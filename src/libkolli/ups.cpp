#include "service_registry.hpp"
#include "curl_session.hpp"
#include <nlohmann/json.hpp>
#include <date/date.h>
#include <sstream>

namespace
{
  using namespace kolli;

  std::chrono::system_clock::time_point parseDate(std::string const & str)
  {
    std::istringstream in(str);
    date::sys_seconds tp;
    in >> date::parse("%d/%m/%Y %R", tp);
    return tp;
  }

  Event getEventInfo(nlohmann::json const & json)
  {
    return Event{
      .description = json["activityScan"],
      .date = parseDate(std::string(json["date"]) + " " + std::string(json["time"])),
      .location = {
        .name = json["location"]
      },
    };
  }

  std::optional<ParcelInfo> getParcelInfo(nlohmann::json const & json)
  {
    ParcelInfo p;
    for (auto const & d : json["trackDetails"])
      for (auto const& e : d["shipmentProgressActivities"])
      p.events.push_back(getEventInfo(e));
    std::reverse(p.events.begin(), p.events.end());
    return p;
  }

  std::vector<std::string> makeHeaders(std::vector<Cookie> const& cookies)
  {
    std::vector<std::string> headers;

    // the XSRF token seems to be required as a header too, the cookie isn't enough
    if (auto it = std::ranges::find(cookies, "X-XSRF-TOKEN-ST", &Cookie::name); it != cookies.end())
      headers.push_back("X-XSRF-TOKEN: " + it->value);

    headers.push_back("Content-Type: application/json");
    // request fails without user-agent
    headers.push_back("user-agent: kolli");

    return headers;
  }

  std::string makePostData(std::string const& trackingID, std::string const& locale)
  {
    return nlohmann::json{
      { "Locale", locale },
      { "TrackingNumber", { trackingID }},
      { "Requester", "st/trackdetails"},
      { "returnToValue", "" },
    }.dump();
  }

  std::optional<ParcelInfo> upsFetch(std::string const & trackingID)
  {
    try
    {
      CurlSession session;

      // fetch tracking page to retrieve the necessary XSRF cookies
      session.get("https://www.ups.com/track?requester=ST/trackdetails&trackNums=" + trackingID, {});

      // locale affects timestamp format in the response, as well as the description strings,
      // the en_US timezone has "A.M./P.M." in the reply, which is somewhat less convenient to parse
      auto const response = session.post("https://www.ups.com/track/api/Track/GetStatus?loc=en_GB",
                                         makePostData(trackingID, "en_GB"),
                                         makeHeaders(session.getCookies()));

      return getParcelInfo(nlohmann::json::parse(response));
    }
    catch (...)
    {
      return {};
    }
  }
}

namespace kolli {
  [[gnu::used]] ServiceRegistration ups_service("ups", upsFetch);
}

#include "service_registry.hpp"
#include "curl_session.hpp"
#include <nlohmann/json.hpp>

namespace
{
  using namespace kolli;

  std::optional<nlohmann::json> asOptional(nlohmann::json const& json, std::string_view field)
  {
    auto it = json.find(field);

    if (it != json.end() && !it->is_null())
      return *it;

    return {};
  }

  std::string getStatusMessage(nlohmann::json const& json)
  {
    std::string result;

    result.append("[");
    // carrier name
    result.append(json["slug"]);
    result.append("] ");
    result.append(json["message"]);

    return result;
  }

  std::string getLocationString(nlohmann::json const& address)
  {
    if (!address.is_null())
    {
      if (auto country = asOptional(address, "country"))
      {
        if (auto city = asOptional(address, "city"))
          return std::string{*city} + ", " + std::string{*country};
        return *country;
      }

      if (auto rawLocation = asOptional(address, "raw_location"))
        return *rawLocation;
    }

    return "N/A";
  }

  Event getEventInfo(nlohmann::json const & json)
  {
    return Event{
      .description = getStatusMessage(json),
      .date = parseDate(json["date_time"]),
      .location = {
        .name = getLocationString(json["address"])
      },
    };
  }


  std::optional<ParcelInfo> getParcelInfo(nlohmann::json const & json)
  {
    ParcelInfo p;
    for (auto const & e : json["data"]["direct_trackings"][0]["tracking"]["checkpoints"])
      p.events.push_back(getEventInfo(e));
    return p;
  }

  std::string getPostData(std::string const& trackingID)
  {
    return R"({"direct_trackings":[{"tracking_number":")" + trackingID + R"(","additional_fields":{},"slug":""}],"translate_to":"en"})";
  }

  std::optional<ParcelInfo> aftershipFetch(std::string const & trackingID)
  {
    try
    {
      return getParcelInfo(nlohmann::json::parse(CurlSession{}.post("https://track.aftership.com/api/v2/direct-trackings/batch",
                                                                    getPostData(trackingID),
                                                                    { "Content-Type: application/json" })));
    }
    catch (...)
    {
      return {};
    }
  }
}

namespace kolli {
  [[gnu::used]] ServiceRegistration aftership_service("aftership", aftershipFetch);
}

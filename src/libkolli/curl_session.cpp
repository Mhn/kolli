#include "curl_session.hpp"
#include <curl/curl.h>
#include <ranges>

namespace
{
  struct CurlFreeSList
  {
    void operator()(curl_slist * slist) const { curl_slist_free_all(slist); }
  };

  class CurlSList
  {
    public:

      CurlSList() = default;
      CurlSList(curl_slist * slist) : m_list(slist) {}

      void append(char const * str)
      {
        m_list.reset(curl_slist_append(m_list.release(), str));
      }

      void append(std::string const& str)
      {
        append(str.c_str());
      }

      explicit operator bool() const
      {
        return m_list.get();
      }

      curl_slist const * get() const { return m_list.get(); }
    private:
      std::unique_ptr<curl_slist, CurlFreeSList> m_list;
  };

  CurlSList makeSListFromStrings(std::ranges::range auto const& container)
  {
    CurlSList list;
    for (auto const& s : container)
      list.append(s);
    return list;
  }

  size_t appendToString(void *data, size_t size, size_t nmemb, void *userp)
  {
    auto s = static_cast<std::string*>(userp);
    s->append(static_cast<char*>(data), size * nmemb);
    return size * nmemb;
  };

  std::vector<std::string_view> split(std::string_view strv, std::string_view delims)
  {
    std::vector<std::string_view> output;
    size_t first = 0;

    while (first < strv.size())
    {
      auto const second = strv.find_first_of(delims, first);

      if (first != second)
        output.emplace_back(strv.substr(first, second - first));

      if (second == std::string_view::npos)
        break;

      first = second + 1;
    }

    return output;
  }

  kolli::Cookie parseCookie(std::string_view cookieStr)
  {
    auto const parts = split(cookieStr, "\t");

    // from curl docs:
    // 0. string example.com - the domain name
    // 1. boolean FALSE - include subdomains
    // 2. string /foobar/ - path
    // 3. boolean TRUE - send/receive over HTTPS only
    // 4. number 1462299217 - expires at - seconds since Jan 1st 1970, or 0
    // 5. string person - name of the cookie
    // 6. string daniel - value of the cookie
    if (parts.size() != 7)
      throw std::runtime_error("Invalid cookie string");

    return kolli::Cookie{
      .name = std::string(parts[5]),
      .value = std::string(parts[6])
    };
  }
}

namespace kolli
{
  void CurlSession::CurlCleanup::operator()(CURL * curl) const
  {
    curl_easy_cleanup(curl);
  }

  CurlSession::CurlSession()
    : m_handle(curl_easy_init())
  {
    // enable the cookie engine
    curl_easy_setopt(m_handle.get(), CURLOPT_COOKIEFILE, "");
  }

  std::string CurlSession::get(std::string const & url, std::vector<std::string> const & headers)
  {
    reset();

    std::string response;

    auto hdrs = makeSListFromStrings(headers);
    curl_easy_setopt(m_handle.get(), CURLOPT_HTTPHEADER, hdrs.get());
    curl_easy_setopt(m_handle.get(), CURLOPT_URL, url.c_str());
    curl_easy_setopt(m_handle.get(), CURLOPT_WRITEFUNCTION, appendToString);
    curl_easy_setopt(m_handle.get(), CURLOPT_WRITEDATA, &response);
    curl_easy_perform(m_handle.get());

    return response;
  }

  std::string CurlSession::post(std::string const & url, std::string const& postData, std::vector<std::string> const & headers)
  {
    reset();

    std::string response;

    auto hdrs = makeSListFromStrings(headers);
    curl_easy_setopt(m_handle.get(), CURLOPT_HTTPHEADER, hdrs.get());
    curl_easy_setopt(m_handle.get(), CURLOPT_URL, url.c_str());
    curl_easy_setopt(m_handle.get(), CURLOPT_WRITEFUNCTION, appendToString);
    curl_easy_setopt(m_handle.get(), CURLOPT_WRITEDATA, &response);
    curl_easy_setopt(m_handle.get(), CURLOPT_POSTFIELDS, postData.c_str());
    curl_easy_perform(m_handle.get());

    return response;
  }

  std::vector<Cookie> CurlSession::getCookies() const
  {
    curl_slist * temp = nullptr;
    auto res = curl_easy_getinfo(m_handle.get(), CURLINFO_COOKIELIST, &temp);

    // take ownership of slist for exception safety
    CurlSList cookies(temp);

    std::vector<Cookie> result;
    if(!res && cookies)
    {
      curl_slist const * it = cookies.get();
      while (it)
      {
        result.push_back(parseCookie(it->data));
        it = it->next;
      }
    }

    return result;
  }

  void CurlSession::reset()
  {
    // reset context options, preserves cookies and connections
    curl_easy_reset(m_handle.get());
  }
}


#include "service_registry.hpp"
#include <nlohmann/json.hpp>

namespace kolli
{
  static std::string buildUrl(std::string const & id)
  {
    return "https://api2.postnord.com/rest/shipment/v5/trackandtrace/ntt/shipment/recipientview?locale=en&id=" + id;
  }

  static Event getEventInfo(nlohmann::json const & json)
  {
    return Event{
      .description = json["eventDescription"],
      .date = parseDate(json["eventTime"]),
      .location = {
        .name = json["location"].contains("displayName") ? json["location"]["displayName"] : "",
      },
    };
  }

  static std::optional<ParcelInfo> getParcelInfo(nlohmann::json const & json)
  {
    ParcelInfo p;
    for (auto const & s : json["TrackingInformationResponse"]["shipments"])
    {
      p.id = s["shipmentId"];
      if (s.contains("estimatedTimeOfArrival"))
        p.estimatedArrival = parseDate(s["estimatedTimeOfArrival"]);
      else if (s.contains("deliveryDate"))
        p.estimatedArrival = parseDate(s["deliveryDate"]);
      for (auto const & i : s["items"])
        for (auto const & e : i["events"])
          p.events.push_back(getEventInfo(e));
    }
    return p;
  }

  static std::optional<ParcelInfo> fetch(std::string const & trackingID)
  {
    try
    {
      return getParcelInfo(nlohmann::json::parse(fetchUrl(buildUrl(trackingID), { "x-bap-key: web-ncp" })));
    }
    catch (...)
    {
      return {};
    }
  }

  [[gnu::used]] ServiceRegistration postnord_service("postnord", fetch);
}

#include <libkolli/service.hpp>
#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>

auto const reset = "\x1B[0m";
auto const on = "\x1B[1;37m";
auto const off = "\x1B[1;30m";

struct Query
{
  std::string service;
  std::string id;
  std::optional<kolli::ParcelInfo> reference_parcel;
  std::optional<kolli::ParcelInfo> latest_parcel;
  std::string output;
};

std::pair<std::string, std::string> splitString(std::string const & str)
{
  std::istringstream f(str);
  std::string service, id;
  std::getline(f, service, ':');
  std::getline(f, id, ':');
  return { service, id };
}

int main(int argc, char *argv[])
{
  std::vector<Query> queries;

  for (int i = 1; i < argc; i++)
  {
    auto [ service, id ] = splitString(argv[i]);
    std::cout << "Service: " << service << " id: " << id << "\n";
    queries.push_back({
        .service = service,
        .id = id,
        });
  }

  while (true)
  {
    std::ostringstream stream;
    bool hasChanges{false};

    for (auto & q : queries)
    {
      if (auto const parcel = kolli::fetch(q.service, q.id))
      {
        if (*parcel != q.latest_parcel.value_or(kolli::ParcelInfo()) || !q.reference_parcel)
        {
          auto newEvents = q.reference_parcel ? *parcel - *q.reference_parcel : kolli::ParcelInfo();
          auto oldEvents = *parcel - newEvents;

          hasChanges = true;

          std::ostringstream ostream;
          ostream << on << kolli::printParcelInfo(*parcel) << reset;
          if (!oldEvents.events.empty())
            ostream << off << kolli::printParcelEvents(oldEvents) << reset;
          if (!newEvents.events.empty())
            ostream << on << kolli::printParcelEvents(newEvents) << reset;
          ostream << "\n";

          q.output = ostream.str();
          q.latest_parcel = parcel;
          if (!q.reference_parcel)
            q.reference_parcel = parcel;
        }
      }
      stream << q.output;
    }

    if (hasChanges)
      std::cout << "\033[2J\033[1;1H" << stream.str();
    std::this_thread::sleep_for(std::chrono::seconds(600));
  }

  return 0;
}
